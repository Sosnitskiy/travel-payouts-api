from flask import Flask

import project
app = project.create_app()

if __name__ == '__main__':
    app.run(host='0.0.0.0')