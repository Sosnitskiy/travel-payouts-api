from flask import Blueprint, render_template, request, redirect, url_for, flash, session
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required
from project import db
from project.models import User, Context_csv

auth = Blueprint('auth', __name__)

@auth.context_processor
def additional_context():
    #context_csv = Context_csv.query.filter_by(file_id=0).count()
    def count_buffer():
        return Context_csv.query.filter_by(file_id=0).count()
    return dict(count_buffer=count_buffer)

@auth.route('/login')
def login():
    return render_template('login.html')


@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(name=email).first()

    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.', 'errors')
        return redirect(url_for('auth.login'))  # if user doesn't exist or password is wrong, reload the page
    else:
        # if the above check passes, then we know the user has the right credentials
        user.authenticated = True
        db.session.add(user)
        db.session.commit()
        login_user(user, remember=remember)
        return redirect(url_for('main.index'))


@auth.route('/signup')
def signup():
    return 'Signup'

@auth.route('/logout')
def logout():
    logout_user()
    if session.get('was_once_logged_in'):
        # prevent flashing automatically logged out message
        del session['was_once_logged_in']
    return redirect(url_for('main.index'))