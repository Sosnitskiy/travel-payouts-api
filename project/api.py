from flask import Blueprint, jsonify
from flask_restful import Api, fields, Resource
from flask_restful import reqparse, abort
from project.models import Destinations
from project.utilites import AlchemyEncoder
import json
from project import db
from sqlalchemy import exc

api = Blueprint('api', __name__, url_prefix='/api')
api_b = Api(api)


def abort_if_notexist(dest_id):
    res = Destinations.query.filter_by(id=dest_id).first()
    if not res:
        abort(404, message="Destination '{}' doesn't exist".format(dest_id))


class DestinationList(Resource):
    def get(self, *args, **kwargs):
        dest = Destinations.query.all()
        data = json.dumps(dest, cls=AlchemyEncoder)
        data_list = list()
        for row in dest:
            data_list.append({"name": row.name,
                              "iata_code": row.iata_code,
                              "DT_RowId": row.id
                              })
        resp = {"draw": 1,
                "recordsTotal": 3,
                "recordsFiltered": 3,
                "data": data_list
                }
        return resp


class Destination(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('name', type=str, location='json')
        self.reqparse.add_argument('iata_code', type=str, location='json')
        super(Destination, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        flag = True
        error = ""
        try:
            dest = Destinations(name=args['name'], iata_code=args['iata_code'])
            db.session.add(dest)
            db.session.commit()
        except exc.IntegrityError as ex:
            error = str(ex)
            db.session.rollback()
            flag = False
        if flag:
            return {"status": "OK"}, 201
        else:
            return {"status": "Error", 'error message': error}, 409


class DestinationDel(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('id', type=str, location='json')
        super(Destination, self).__init__()

    def delete(self, dest_id):
        args = self.reqparse.parse_args()
        abort_if_notexist(args["id"])
        return {"status": "OK"}, 204


api_b.add_resource(DestinationList, '/destination/')
api_b.add_resource(Destination, '/destination/add')
api_b.add_resource(DestinationDel, '/destination/del/<dest_id>')



