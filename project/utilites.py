from sqlalchemy.ext.declarative import DeclarativeMeta
import json
from project.models import Weekdays, Destinations, File_csv, Context_csv


def load_day_of_weeks():
    result = dict()
    for el in Weekdays.query.all():
        result[el.name] = el.hebr_day
    return result

def load_iata_to_name():
    result = dict()
    for el in Destinations.query.all():
        result[el.iata_code] = el.name
    return result

def get_dey_of_week(hedr_days, data):
    result = None
    dof = data.strftime("%A").lower()
    result = hedr_days.get(dof, None)
    return result

def name_generator(origin_airport, destination_airport, departure_date, return_date, price, direct):
    res = "{0}-{1} {2}-{3} | ${4}".format(origin_airport,
                                       destination_airport,
                                       departure_date.strftime("%Y/%m/%d"),
                                       return_date.strftime("%Y/%m/%d") if return_date else "",
                                       price
                                       )
    if direct in (True, False):
        res += " | {0}".format("DIRECT" if direct else "NONE DIRECT")

    return res

def key_generator(origin_airport, destination_airport, departure_date, return_date, price):
    res = "{0}-{1}-{2}-{3}-{4}".format(origin_airport,
                                   destination_airport,
                                   departure_date.strftime("%Y%m%d%H%M"),
                                   return_date.strftime("%Y%m%d%H%M"),
                                   price
                                   )
    return res

def key_generator_v3(direct, origin_airport, destination_airport, departure_date, return_date, price, transfers, return_transfers):
    direct_calc = "DIRECT"
    if transfers or return_transfers:
        direct_calc = "NON_DIRECT"
    res = "{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}".format(origin_airport,
                                           destination_airport,
                                           departure_date.strftime("%Y%m%d%H%M"),
                                           return_date.strftime("%Y%m%d%H%M"),
                                           price,
                                           direct_calc,
                                           transfers,
                                           return_transfers
                                   )
    return res

class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)