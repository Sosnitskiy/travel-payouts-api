'''

https://api.travelpayouts.com/aviasales/v3/prices_for_dates?origin=string&destination=BCN&departure_at=string&return_at=string&unique=false&sorting=price&direct=false&currency=rub&limit=30&page=1&one_way=true&token=PutYourTokenHere&_gl=1*h7glfc*_ga*MTA2ODQyNjY4NC4xNjU3ODU3ODEw*_ga_1WLL0NEBEH*MTY1ODI0ODA0Ni41LjAuMTY1ODI0ODA0Ni42MA..
'''
import time
from random import randint

import requests
import urllib.parse
import datetime
from project import db
from project.models import Request, Response
from project.custom_errors import ToManyRetry
from flask import flash


class AviasalesData:
    def __init__(self, currency, origin, destination, departure_at, return_at, direct, limit, page, sorting, unique, req):
        self.currency = currency
        self.origin = origin
        self.destination = destination
        self.departure_at = departure_at
        self.return_at = return_at
        self.direct = direct
        self.limit = limit
        self.page = page
        self.sorting = sorting
        self.unique = unique
        self.full_resp = None
        self.data = list()
        self.req = req
        self.request_id = None
        self.req_urls = list()

    def __get_token(self):
        return "1243883422dd391b8f059331749fca8c"

    def __generate_params(self):
        params = dict()
        if self.currency:
            params['currency'] = self.currency
        if self.origin:
            params['origin'] = self.origin
        if self.destination:
            params['destination'] = self.destination
        if self.departure_at:
            params['departure_at'] = self.departure_at
        if self.return_at:
            params['return_at'] = self.return_at
        if self.direct:
            params['direct'] = self.direct
        if self.limit:
            params['limit'] = self.limit
        if self.page:
            params['page'] = self.page
        if self.sorting:
            params['sorting'] = self.sorting
        if self.unique:
            params['unique'] = self.unique

        params['token'] = self.__get_token()
        return params

    def create_request(self, url=None):
        #print("   1")
        if not url:
            url = "https://api.travelpayouts.com/aviasales/v3/prices_for_dates"
        params = self.__generate_params()
        #print("   2")
        counter_iter = 0
        while True:
            #print("   3")
            counter_iter +=1
            full_resp = requests.get(url, params=params, timeout=20)
            #print("   4")
            if full_resp.status_code >= 200 and full_resp.status_code <=300:
                self.req_urls.append(full_resp.url)
                try:
                    data = full_resp.json()
                    #print(data)
                except Exception as ex:
                    print(ex)
                    dd = 0
                if not data['success']:
                    print(data)
                    raise Exception(data['error'])
                self.data = self.data + data["data"]

                if len(data.get("data")) < self.limit:
                    break
                else:
                    params["page"] +=1
                #print("         5_1")
            elif full_resp.status_code == 429:
                print("____ sleep ____")
                print(full_resp.status_code)
                raise ToManyRetry(full_resp.status_code)
            elif full_resp.status_code >= 400 and full_resp.status_code <=500:
                print("PARAMS")
                print(params)
                print("RESPONSE")
                print(full_resp)
                #print("         5_2")
                raise Exception(full_resp.text)
                break
                #print("         5_3")
            #print("   6")

    def to_table(self):
        pass

    def write_to_db_request(self, data):
        req = Request(origin_airport=self.origin if self.origin else None,
                      destination_airport=self.destination if self.destination else None,
                      t_start=data['t-start'] if data.get('t-start', False) else None,
                      t_end=data['t-end'] if data.get('t-end', False) else None,
                      max_price=data['max_price'] if data.get('max_price', False) else None,
                      transfers=data['transfers'] if data.get('transfers', False) else None,
                      return_transfers=data['return_transfers'] if data.get('return_transfers', False) else None,
                      min_nights_duration=data['min_nights_duration'] if data.get('min_nights_duration',
                                                                                  False) else None,
                      max_nights_duration=data['max_nights_duration'] if data.get('max_nights_duration',
                                                                                  False) else None)
        db.session.add(req)
        db.session.commit()
        self.request_id = req.id

    def ext_validate_data(self, row, req_data):
        extend_validation = list()

        if req_data.get("max_price", False):
            extend_validation.append(req_data.get("max_price") >= row.get('price', 0))
        if req_data.get('transfers', False) or req_data.get('transfers') == 0:
            extend_validation.append(req_data.get("transfers") == row.get('transfers', 0))
        if req_data.get('return_transfers', False) or req_data.get('return_transfers') == 0:
            extend_validation.append(req_data.get('return_transfers') == row.get('return_transfers', 0))
        if req_data.get('min_nights_duration', False):
            if row.get('return_at', False):
                return_at = datetime.datetime.strptime(row.get('return_at'), "%Y-%m-%dT%H:%M:%S%z")
                departure_at = datetime.datetime.strptime(row.get('departure_at'), "%Y-%m-%dT%H:%M:%S%z")
                nights = abs((return_at - departure_at).days)
            else:
                nights = 0
            extend_validation.append(nights >= int(req_data.get('min_nights_duration', 0)))
        if req_data.get('max_nights_duration', False):
            if row.get('return_at', False):
                return_at = datetime.datetime.strptime(row.get('return_at'), "%Y-%m-%dT%H:%M:%S%z")
                departure_at = datetime.datetime.strptime(row.get('departure_at'), "%Y-%m-%dT%H:%M:%S%z")
                nights = abs((return_at - departure_at).days)
            else:
                nights = 0
            extend_validation.append(nights <= int(req_data.get('max_nights_duration', 0)))

        row["validation"] = all(extend_validation)
        return row["validation"]

    def write_to_db_response(self, req_data):
        Response.query.delete()
        count = 0
        for row in self.data:
            extend_validation = self.ext_validate_data(row, req_data)
            if extend_validation:
                url_data = urllib.parse.urljoin('https://www.aviasales.com/search/', row.get('link'))
                resp_row = Response(origin=row.get('origin'),
                                    destination=row.get('destination'),
                                    origin_airport=row.get('origin_airport'),
                                    destination_airport=row.get('destination_airport'),
                                    price=row.get('price'),
                                    airline=row.get('airline'),
                                    flight_number=row.get('flight_number'),
                                    departure_at=datetime.datetime.strptime(row.get('departure_at'), "%Y-%m-%dT%H:%M:%S%z") if row.get('departure_at', None) else None,
                                    return_at=datetime.datetime.strptime(row.get('return_at'), "%Y-%m-%dT%H:%M:%S%z") if row.get('return_at', None) else None,
                                    transfers=row.get('transfers'),
                                    return_transfers=row.get('return_transfers'),
                                    duration=row.get('duration'),
                                    link=url_data,
                                    currency=self.currency,
                                    request_id=self.request_id
                                    )
                db.session.add(resp_row)
        db.session.commit()


    @classmethod
    def fill_from_forms(cls, data):
        data_for_fill = {
                            'currency': 'USD',
                            'origin': '',
                            'destination': '',
                            'departure_at': "",
                            'return_at': "",
                            "direct": False,
                            'limit': 100,
                            'page': 1,
                            "sorting": "price",
                            "unique": False,
                            'req': data
        }
        for el in data:
            if el == 'origin_airport':
                data_for_fill['origin'] = data['origin_airport']
            elif el == 'destination_airport':
                data_for_fill['destination'] = data['destination_airport']
            elif el == 't-start':
                data_for_fill['departure_at'] = data['t-start'].strftime('%Y-%m-%d')
            elif el == 't-end':
                data_for_fill['return_at'] = data['t-end'].strftime('%Y-%m-%d')

        return cls(**data_for_fill)