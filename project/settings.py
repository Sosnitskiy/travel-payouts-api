from flask import request
from flask import redirect, url_for
from project.models import Destinations
from sqlalchemy import or_
from flask import Blueprint, render_template, flash
from project import db
from flask_login import login_required
from project.models import Weekdays, Context_csv
from sqlalchemy import exc
from sqlalchemy import desc, asc

setting = Blueprint('setting', __name__)

@setting.context_processor
def additional_context():
    #context_csv = Context_csv.query.filter_by(file_id=0).count()
    def count_buffer():
        return Context_csv.query.filter_by(file_id=0).count()
    return dict(count_buffer=count_buffer)

@setting.route('/destitations')
@login_required
def destitations():
    return render_template('destitations.html')


@setting.route('/week_days')
@login_required
def week_days():
    days = Weekdays.query.all()
    return render_template('week_days.html' ,days=days)

@login_required
@setting.route('/add_destination', methods=["POST"])
def add_destination():
    iata = request.form['iata']
    des_name = request.form['des_name']

    if not iata:
        flash('IATA is required!','error')
    elif not des_name:
        flash('Name is required!', 'error')
    else:
        try:
            dest = Destinations(name=des_name, iata_code=iata)
            db.session.add(dest)
            db.session.commit()
        except exc.IntegrityError as ex:
            error = str(ex)
            db.session.rollback()
            flash(error, 'error')
    return redirect(url_for('setting.destitations'))


@login_required
@setting.route('/del_destination/<id>', methods=["DELETE"])
def del_destinations(id):
    dest = Destinations.query.get(id)
    db.session.delete(dest)
    db.session.commit()
    return "Destinations was successfully deleted"

@login_required
@setting.route('/get_destinations')
def get_destinations():
    args = request.args
    search = args.get("search[value]", False)
    length = request.args.get('length', 10, type=int)
    start = request.args.get('start', 0, type=int)
    page = int((length+start)/length)
    order_field_num = request.args.get('order[0][column]', None, type=int)
    order_direction = request.args.get('order[0][dir]', None)
    count_rec = Destinations.query.count()
    if search:
        dest = Destinations.query.filter(or_(Destinations.name.ilike('%{}%'.format(search)),
                                             Destinations.iata_code.ilike('%{}%'.format(search))
                                             ))
    else :
        dest = Destinations.query
    if order_field_num:
        key_name = 'columns[{}][data]'.format(order_field_num)
        order_field_name = request.args.get(key_name)
        if order_field_name == "departure_date":
            order_field_name = 'departure_at'
        elif order_field_name == 'return_date':
            order_field_name = 'return_at'
        sort = asc(order_field_name) if order_direction == "desc" else desc(order_field_name)
        dest = dest.order_by(sort)
    data_rows = dest.paginate(page, max_per_page=length)
    data_list = list()
    for row in data_rows.items:
        data_list.append({"name": row.name,
                          "iata_code": row.iata_code,
                          "v3_enable": row.v3_enable,
                          "DT_RowId": row.id
                          })
    result = dict(data=data_list,
                  recordsTotal=data_rows.total,
                  recordsFiltered=data_rows.total,
                  current_page=data_rows.page,
                  per_page=data_rows.per_page,
                  )
    return result

