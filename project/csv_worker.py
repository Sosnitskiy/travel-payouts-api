from flask import Blueprint, render_template, request, flash, Response
from flask import abort
from project import db
from flask_login import login_required
from project.models import Destinations, Context_csv, File_csv, Response as Response_m
from sqlalchemy import or_
from sqlalchemy import desc, asc
from sqlalchemy import exc
from project.utilites import load_iata_to_name, load_day_of_weeks
from project.utilites import name_generator, get_dey_of_week
import datetime
import urllib

csv_worker = Blueprint('csv_worker', __name__)

@csv_worker.context_processor
def additional_context():
    #context_csv = Context_csv.query.filter_by(file_id=0).count()
    def count_buffer():
        return Context_csv.query.filter_by(file_id=0).count()
    return dict(count_buffer=count_buffer)

@login_required
@csv_worker.route('/move_data_export', methods=['POST'])
def move_data_export():
    file_name = request.values.get('file_name')
    save_new_file = request.values.get('save_new_file')
    if save_new_file == 'true':
        save_new_file = True
    else:
        save_new_file = False
    ids = request.values.get('ids')
    if ids:
        ids = ids.split("|")
    maincsv = request.values.get('maincsv')
    if maincsv == 'asnew':
        if not file_name:
            file_name = datetime.datetime.now().strftime("%Y%m%d_%H_%M")+".csv"
        file_header = File_csv.query.filter_by(name=file_name).first()
        if not file_header:
            file_header = File_csv(name = file_name)
            db.session.add(file_header)
            db.session.commit()
        for id in ids:
            row = Context_csv.query.get(id)
            row.file_id = file_header.id
            db.session.add(row)
            db.session.commit()

    if maincsv =='mainAdd':
        file_header = File_csv.query.get(1)
        for id in ids:
            try:
                row = Context_csv.query.get(id)
                row.file_id = file_header.id
                db.session.add(row)
                db.session.commit()
            except exc.IntegrityError as ex:
                db.session.rollback()
                db.session.delete(row)
                db.session.commit()
            except Exception as ex:
                dd = 0

    if maincsv == 'mainOverwrite':
        file_header = File_csv.query.get(1)
        for row in Context_csv.query.filter_by(file_id=1).all():
            db.session.delete(row)
        db.session.commit()
        for id in ids:
            row = Context_csv.query.get(id)
            row.file_id = file_header.id
            db.session.add(row)
            db.session.commit()
    return ""

@login_required
@csv_worker.route('/del_exported_row/<id>', methods=['DELETE'])
def del_exported_row(id):
    row = Context_csv.query.get(id)
    db.session.delete(row)
    db.session.commit()
    return "Destinations was successfully deleted"

@login_required
@csv_worker.route('/del_deleterow2/<id>', methods=['DELETE'])
def del_deleterow2(id):
    row = Context_csv.query.get(id)
    db.session.delete(row)
    db.session.commit()
    return "Destinations was successfully deleted"

@login_required
@csv_worker.route('/get_csv_files_internal', methods=['POST'])
def get_csv_files_internal():

    hebr_days = load_day_of_weeks()
    iata_names = load_iata_to_name()
    search = request.form.get("search[value]", False)
    length = request.form.get('length', 10, type=int)
    start = request.form.get('start', 0, type=int)
    parent_id = request.form.get('ParentId', 1, type=int)
    page = int((length + start) / length)
    order_field_num = request.form.get('order[0][column]', None, type=int)
    order_direction = request.form.get('order[0][dir]', None)
    count_rec = Context_csv.query.filter_by(file_id=parent_id).count()
    if search:
        dest = Context_csv.query.filter(or_(Context_csv.origin.ilike('%{}%'.format(search)),
                                              Context_csv.destination.ilike('%{}%'.format(search)),
                                              Context_csv.origin_airport.ilike('%{}%'.format(search)),
                                              Context_csv.destination_airport.ilike('%{}%'.format(search)),
                                              Context_csv.airline.ilike('%{}%'.format(search)),
                                                  ))
        dest = dest.filter_by(file_id=parent_id)
    else:
        dest = Context_csv.query.filter_by(file_id=parent_id)

    if order_field_num:
        key_name = 'columns[{}][data]'.format(order_field_num)
        order_field_name = request.form.get(key_name)
        if order_field_name == "departure_date":
            order_field_name = 'departure_at'
        elif order_field_name == 'return_date':
            order_field_name = 'return_at'
        sort = asc(order_field_name) if order_direction == "desc" else desc(order_field_name)
        dest = dest.order_by(sort)
    data_rows = dest.paginate(page, per_page=length)
    data_list = list()
    for row in data_rows.items:
        name_row = name_generator(row.origin_airport,
                                  row.destination_airport,
                                  row.departure_at,
                                  row.return_at,
                                  row.price,
                                  row.direct)
        data_list.append({"name": name_row,
                          "origin_airport": row.origin_airport,
                          "destination_airport": row.destination_airport,
                          "destination_name": iata_names.get(row.destination, "--"),
                          "departure_date": row.departure_at.strftime("%Y/%m/%d %H:%M"),
                          "departure_dayoftheweek": get_dey_of_week(hebr_days, row.departure_at),
                          "return_date": row.return_at.strftime("%Y/%m/%d %H:%M"),
                          "return_dayoftheweek": get_dey_of_week(hebr_days, row.return_at),
                          "total_nights": abs((row.return_at - row.departure_at).days),
                          "price": row.price,
                          "direct": row.direct,
                          "DT_RowId": row.id
                          })
    result = dict(data=data_list,
                  recordsTotal=count_rec,
                  recordsFiltered=data_rows.total,
                  current_page=data_rows.page,
                  per_page=data_rows.per_page,
                  )
    return result

@login_required
@csv_worker.route('/del_csv_files_list/<id>', methods=['DELETE'])
def del_csv_files_list(id):
    row = File_csv.query.get(id)
    db.session.delete(row)
    db.session.commit()
    return "Destinations was successfully deleted"





@login_required
@csv_worker.route('/get_csv_files_list')
def get_csv_files_list():

    args = request.args
    search = args.get("search[value]", False)
    length = request.args.get('length', 10, type=int)
    start = request.args.get('start', 0, type=int)
    page = int((length + start) / length)
    count_rec = File_csv.query.count()
    order_field_num = request.args.get('order[0][column]', None, type=int)
    order_direction = request.args.get('order[0][dir]', None)
    if search:
        dest = File_csv.query.filter(File_csv.name.ilike('%{}%'.format(search)))
    else:
        dest = File_csv.query
    dest = dest.filter(File_csv.name != 'buffer')
    if order_field_num !=None:
        key_name = 'columns[{}][data]'.format(order_field_num)
        order_field_name = request.args.get(key_name)
        sort = asc(order_field_name) if order_direction == "desc" else desc(order_field_name)
        dest = dest.order_by(sort)
    data_rows = dest.paginate(page, per_page=length)
    data_list = list()
    for row in data_rows.items:
        data_list.append({"name": row.name,
                          "DT_RowId": row.id
                          })
    result = dict(data=data_list,
                  recordsTotal=count_rec,
                  recordsFiltered=data_rows.total,
                  current_page=data_rows.page,
                  per_page=data_rows.per_page,
                  )
    return result


@login_required
@csv_worker.route('/flights_export_csv')
def flights_export():
    return render_template('flights_export.html')


@login_required
@csv_worker.route('/flights_csv_files')
def csv_files():
    return render_template('flights_csv_files.html')

@login_required
@csv_worker.route('/get_export_url_for_test/<type_search>/<id>')
def get_export_url_for_test(type_search, id):
    '''
    https://okbye.travel/wp-content/themes/hello-theme-child-master/to_flights.php?
    destination_iata=[destination_airport]&origin_iata=[origin_airport]&infants=0
    &locale=he&one_way=false&trip_class=0&with_request=true&acc_date_in=&acc_date_out=
    &t-start=[departure_date]&t-end=[return_date]&adultNumber=1&childrenNumber=0
    :param id:
    :return:
    '''
    if type_search == "search":
        export_row = Response_m.query.get(id)
    else:
        export_row = Context_csv.query.get(id)
    link = export_row.link
    '''new_params = {
        'destination_iata': export_row.destination_airport if export_row.destination_airport else "",
        'origin_iata': export_row.origin_airport if export_row.origin_airport else "",
        'infants': 0,
        'locale':'he',
        'one_way':'false',
        'trip_class': 0,
        'with_request': 'true',
        'acc_date_in': '',
        'acc_date_out':'',
        't-start': export_row.departure_at if export_row.departure_at else "",
        't-end': export_row.return_at if export_row.return_at else "",
        'adultNumber': 1,
        'childrenNumber': 0
    }
    params_str = urllib.parse.urlencode(new_params)
    url_str = 'https://okbye.travel/wp-content/themes/hello-theme-child-master/to_flights.php?'
    link = url_str+params_str'''
    return link

@csv_worker.route("/files/<file_name>")
def all_flights(file_name):
    file_rec = File_csv.query.filter_by(name=file_name).first()
    if file_rec:
        csv_arr = list()
        hebr_days = load_day_of_weeks()
        iata_names = load_iata_to_name()
        export_rows = Context_csv.query.filter_by(file_id=file_rec.id).all()
        for export_row in export_rows:
            name_row = name_generator(export_row.origin_airport,
                                      export_row.destination_airport,
                                      export_row.departure_at,
                                      export_row.return_at,
                                      export_row.price,
                                      export_row.direct)
            data = {"name": name_row,
                  "origin_airport": export_row.origin_airport,
                    "origin_name" : iata_names.get(export_row.origin_airport, "--"),
                  "destination_airport": export_row.destination_airport,
                  "destination_name": iata_names.get(export_row.destination, "--"),
                  "departure_date": export_row.departure_at.strftime("%Y/%m/%d"),
                  "departure_time": export_row.departure_at.strftime("%H:%M"),
                  "departure_dayoftheweek": get_dey_of_week(hebr_days, export_row.departure_at),
                  "return_date": export_row.return_at.strftime("%Y/%m/%d"),
                  "return_time": export_row.return_at.strftime("%H:%M"),
                  "return_dayoftheweek": get_dey_of_week(hebr_days, export_row.return_at),
                  "total_nights": abs((export_row.return_at - export_row.departure_at).days),
                  "price": export_row.price,
                  "direct": "",
                  }
            if export_row.direct in (True, False):
                data["direct"]= 1 if export_row.direct else 0
            csv_row = "{name},{origin_airport},{origin_name},{destination_airport},{destination_name},".format(**data)
            csv_row += "{departure_date},{departure_time},{departure_dayoftheweek},{return_date},{return_time},".format(**data)
            csv_row += "{return_dayoftheweek},{total_nights},{price},{direct}".format(**data)
            csv_arr.append(csv_row)
        csv = ("\n").join(csv_arr)
        return Response(
            csv,
            mimetype="text/csv",
            headers={"Content-disposition":
                     "attachment; filename={0}".format(file_name)})
    else:
        abort(404)


@login_required
@csv_worker.route('/get_flights_selected')
def get_flights_selected():
    hebr_days = load_day_of_weeks()
    iata_names = load_iata_to_name()
    args = request.args
    search = args.get("search[value]", False)
    length_ = request.args.get('length', 10, type=int)
    start = request.args.get('start', 0, type=int)
    page = int((length_ + start) / length_)
    #count_rec = Context_csv.query.filter_by(file_id=0).count()
    dest = Context_csv.query.filter_by(file_id=0)
    if search:
        dest = dest.filter(or_(Context_csv.origin.ilike('%{}%'.format(search)),
                                             Context_csv.origin_airport.ilike('%{}%'.format(search)),
                                             Context_csv.destination.ilike('%{}%'.format(search)),
                                             Context_csv.destination_airport.ilike('%{}%'.format(search)),
                                             ))

    data_rows = dest.paginate(page, per_page=length_)
    data_list = list()
    for row in data_rows.items:
        name_row = name_generator(row.origin_airport,
                                  row.destination_airport,
                                  row.departure_at,
                                  row.return_at,
                                  row.price,
                                  row.direct
                                  )
        data_list.append({"name": name_row,
                          "origin_airport": row.origin_airport,
                          "destination_airport": row.destination_airport,
                          "destination_name": iata_names.get(row.destination, "--"),
                          "departure_date": row.departure_at.strftime("%Y/%m/%d %H:%M"),
                          "departure_dayoftheweek": get_dey_of_week(hebr_days, row.departure_at),
                          "return_date": row.return_at.strftime("%Y/%m/%d %H:%M"),
                          "return_dayoftheweek": get_dey_of_week(hebr_days, row.return_at),
                          "total_nights": abs((row.return_at - row.departure_at).days),
                          "price": row.price,
                          "direct": row.direct,
                          "DT_RowId": row.id
                          })
    result = dict(data=data_list,
                  recordsTotal=data_rows.total,
                  recordsFiltered=data_rows.total,
                  current_page=data_rows.page,
                  per_page=data_rows.per_page,
                  )
    return result

