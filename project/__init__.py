from flask import Flask, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_restful import Api
from flask_migrate import Migrate
import socket
from project.celery_utils import get_celery_app_instance
from celery import Celery
from celery.schedules import crontab
import redis

CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'

# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()
migrate = Migrate()
celery = Celery(__name__, broker=CELERY_BROKER_URL, result_backend=CELERY_RESULT_BACKEND)
redis_connect = redis.Redis.from_url( url=CELERY_RESULT_BACKEND)



#celery.conf.update(app.config)
celery.conf.update(enable_utc=False, timezone='Asia/Jerusalem', )
celery.conf.beat_schedule = {
    'week100baks_v2':
        {'task': 'project.tasks.week15_xxx_baks',
         'schedule': crontab(hour="*/1", minute="0"),
         'args': (1,),
         },
    'week52_5000baks_v2':
        {'task': 'project.tasks.week15_xxx_baks',
         'schedule': crontab(hour="0", minute="0"),
         'args': (8,),
         },
    'week26_2000baks_v3':
        {'task': 'project.tasks.week15_xxx_baks',
         'schedule': crontab(hour="2", minute="0"),
         'args': (11,),
         },
}


def create_app():
    app = Flask(__name__,
                static_folder='static')

    app.config['SECRET_KEY'] = 'secret-key-goes-here'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user2:XmLuwoRTaPBB@localhost:5432/travel'

    db.init_app(app)
    migrate.init_app(app, db)

    
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))

    # blueprint for auth routes in our app
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .csv_worker import csv_worker as csv_worker_blueprint
    app.register_blueprint(csv_worker_blueprint)

    from .settings import setting as setting_blueprint
    app.register_blueprint(setting_blueprint)

    from .tasks import task_worker as task_worker_blueprint
    app.register_blueprint(task_worker_blueprint)

    #from .api import api as api_blueprint
    #app.register_blueprint(api_blueprint)


    #celery.autodiscover_tasks()

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask

    return app
