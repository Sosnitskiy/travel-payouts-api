from crypt import methods

from celery.utils.functional import first
from flask import Blueprint, render_template
from flask import request, flash, Response as resp_flask
from flask import jsonify
from project import db
from flask_login import login_required, current_user
from flask import redirect, url_for
from project.models import Destinations, Response, Context_csv, Request
import datetime
from project.flight_tickets_api import AviasalesData
from project.utilites import load_day_of_weeks, load_iata_to_name, get_dey_of_week
from project.utilites import name_generator, key_generator
from sqlalchemy import exc
from functools import wraps
from sqlalchemy import inspect, or_
from sqlalchemy import desc, asc

main = Blueprint('main', __name__)


def calculator_buffer(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        cont = Context_csv.query.filter_by(file_id=0).count()
        dd= 0
        return f(*args, **kwargs)
    return decorated_function

@main.context_processor
def additional_context():
    #context_csv = Context_csv.query.filter_by(file_id=0).count()
    def count_buffer():
        return Context_csv.query.filter_by(file_id=0).count()
    return dict(count_buffer=count_buffer)


@main.route('/', methods=['GET', 'POST'])
@login_required
def index():

    if request.method == "GET":
        #data_form = dict()
        data_form = Request.query.order_by(Request.id.desc()).first()
        if data_form:
            data_form2= {c.key: getattr(data_form, c.key) for c in inspect(data_form).mapper.column_attrs}
        else:
            data_form2 = dict()
        dest = Destinations.query.all()
        for dest_row in dest:
            if dest_row.iata_code == data_form2.get("origin_airport", None):
                data_form2["origin_airport"] = dest_row.name + " | " + dest_row.iata_code
            if dest_row.iata_code == data_form2.get("destination_airport", None):
                data_form2["destination_airport"] = dest_row.name + " | " + dest_row.iata_code
        for data in data_form2:
            if data in ('destination_airport', 'origin_airport') and (data_form2.get(data, None)== "None" or data_form2.get(data, None) == None):
                data_form2[data] = ''
        return render_template('flights_search.html', origin_airport=dest, destination_airport=dest, data_form= (data_form2))
    else:
        data = dict()
        for data_form in request.form:
            if request.form[data_form] != 'null' and len(str(request.form[data_form]))>0:
                if data_form in ('origin_airport', 'destination_airport'):
                    if request.form[data_form] != 'None' and request.form[data_form] !="":
                        full_name_ar = request.form[data_form].split(" | ")
                        if len(full_name_ar)>1:
                            data[data_form] = full_name_ar[1].strip()
                        else:
                            data[data_form] = full_name_ar[0].strip()
                elif data_form in ('max_price','transfers', 'return_transfers'):
                    data[data_form] = int(request.form[data_form])
                elif data_form in ('t-start', 't-end'):
                    dt_conf = datetime.datetime.strptime(request.form[data_form], "%Y-%m-%d").date()
                    data[data_form] = dt_conf
                elif data_form in ('min_nights_duration', 'max_nights_duration'):
                    data[data_form] = int(request.form[data_form])
                else:
                    data[data_form] = request.form[data_form]
        req = AviasalesData.fill_from_forms(data)
        req.write_to_db_request(data)
        try:
            req.create_request()
        except Exception as ex:
            flash(str(ex), 'error')
        else:
            req.write_to_db_response(data)
        dest = Destinations.query.all()
        req_urls = '<div class="list-group">'
        for iter_num, url in enumerate(req.req_urls):
            req_urls += '<a href="{0}" target ="_blank" class="list-group-item list-group-item-action">Request {1}</a>'.format(url, iter_num+1)
        req_urls += '</div>'
        for dest_row in dest:
            if dest_row.iata_code == data.get("origin_airport",  None):
                data["origin_airport"] = dest_row.name + " | " + dest_row.iata_code
            if dest_row.iata_code == data.get("destination_airport", None):
                data["destination_airport"] = dest_row.name + " | " + dest_row.iata_code
        if data.get('t-start'):
            data['t_start'] = data['t-start']
        if data.get('t-end'):
            data['t_end'] = data['t-end']
        return render_template('flights_search.html', origin_airport=dest, destination_airport=dest, req_urls=req_urls, data_form=(data))

@login_required
@main.route('/del_get_flights_search_row/<id>', methods=["DELETE"])
def del_destinations(id):
    resp = Response.query.get(id)
    db.session.delete(resp)
    db.session.commit()
    return "Destinations was successfully deleted"


@login_required
@main.route("/v3_enable/<int:id>", methods=["POST"])
def v3_enable(id):
    resp = resp_flask("success", 202)
    query = Destinations.query.filter_by(id=id).first()
    if query:
        query.v3_enable = False if query.v3_enable else True
        db.session.commit()
    else:
        resp = resp_flask("not found", 422)
    return resp


@login_required
@main.route('/get_flights_search_result', methods=["GET"])
def get_flights_search_result():
    hebr_days =load_day_of_weeks()
    iata_names = load_iata_to_name()
    args = request.args
    search = args.get("search[value]", False)
    length_ = request.args.get('length', 300, type=int)
    start = request.args.get('start', 0, type=int)
    page = int((length_ + start) / length_)
    order_field_num = request.args.get('order[0][column]', None, type=int)
    order_direction = request.args.get('order[0][dir]', None)
    count_rec = Response.query.count()
    if search:
        dest = Response.query.filter(or_(Response.origin.ilike('%{}%'.format(search)),
                                         Response.destination.ilike('%{}%'.format(search)),
                                         Response.origin_airport.ilike('%{}%'.format(search)),
                                         Response.destination_airport.ilike('%{}%'.format(search)),
                                         #Response.price.ilike('%{}%'.format(search)),
                                         Response.airline.ilike('%{}%'.format(search)),
                                         #Response.flight_number.ilike('%{}%'.format(search)),
                                         #Response.departure_at.ilike('%{}%'.format(search)),
                                         #Response.return_at.ilike('%{}%'.format(search)),
                                         #Response.transfers.ilike('%{}%'.format(search)),
                                         #Response.return_transfers.ilike('%{}%'.format(search)),
                                         #Response.duration.ilike('%{}%'.format(search)),
                                             ))
    else:
        dest = Response.query
    if order_field_num:
        key_name = 'columns[{}][data]'.format(order_field_num)
        order_field_name = request.args.get(key_name)
        if order_field_name == "departure_date":
            order_field_name = 'departure_at'
        elif order_field_name == 'return_date':
            order_field_name = 'return_at'
        sort = asc(order_field_name) if order_direction == "desc" else desc(order_field_name)
        dest = dest.order_by(sort)
    data_rows = dest.paginate(page, per_page=length_)
    data_list = list()
    for row in data_rows.items:
        data_list.append({"name": name_generator(row.origin_airport, row.destination_airport, row.departure_at, row.return_at, row.price, None),
                          "origin_airport": row.origin_airport,
                          "destination_airport": row.destination_airport,
                          "destination_name": iata_names.get(row.destination, "--"),
                          "departure_date": row.departure_at.strftime("%Y/%m/%d %H:%M"),
                          "departure_dayoftheweek": get_dey_of_week(hebr_days, row.departure_at),
                          "return_date": row.return_at.strftime("%Y/%m/%d %H:%M") if row.return_at else None,
                          "return_dayoftheweek": get_dey_of_week(hebr_days,row.return_at) if row.return_at else "???",
                          "total_nights": abs((row.return_at - row.departure_at).days) if row.return_at else "???",
                          "price": row.price,
                          "DT_RowId": row.id
                          })
    result = dict(data=data_list,
                  recordsTotal=data_rows.total,
                  recordsFiltered=data_rows.total,
                  current_page=data_rows.page,
                  per_page=data_rows.per_page,
                  )
    return result

@login_required
@main.route('/add_flights_search_result/<ids>', methods=["POST"])
def add_flights_search_result(ids):
    result = dict()
    count_add = 0
    skipped_add = 0
    for id in ids.split("|"):
        try:
            resp = Response.query.get(id)
            cont_csv = Context_csv(origin = resp.origin,
                                   destination = resp.destination,
                                   origin_airport = resp.origin_airport,
                                   destination_airport = resp.destination_airport,
                                   price = resp.price,
                                   airline = resp.airline,
                                   flight_number = resp.flight_number,
                                   departure_at = resp.departure_at,
                                   return_at = resp.return_at,
                                   transfers = resp.transfers,
                                   return_transfers = resp.return_transfers,
                                   duration = resp.duration,
                                   link = resp.link,
                                   currency = resp.currency,
                                   key_unic = key_generator(resp.origin_airport, resp.destination_airport, resp.departure_at, resp.return_at, resp.price),
                                   file_id = 0)
            db.session.add(cont_csv)
            db.session.commit()
            count_add += 1
        except exc.IntegrityError as ex:
            skipped_add+=1
            error = str(ex)
            db.session.rollback()
            #flash(error, 'error')
        #flash("Was added {} records".format(count_add), 'siccess')
        count_data = Context_csv.query.filter_by(file_id=0).count()
        result['total_count_data'] = count_data
        result['added'] = count_add
        result['skipped'] = skipped_add
    return result

@main.route('/profile')
def profile():
    return 'Profile'