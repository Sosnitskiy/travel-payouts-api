
  $(document).ready(function(){

       var table =$('#dataTableExportResult').DataTable({
            processing: false,
            serverSide: true,
            stateSave: true,
            //paging: true,
            pageLength: 300,
            lengthMenu: [ [10, 25, 50, 100, 300], [10, 25, 50, 100, "300"] ],
            pagingType: "full_numbers",
            //info: true,
            //autoWidth: false,
            ajax: '/get_flights_selected',
            columns: [
                    { data: 'name', title:"Name", width: "25%", orderable:false  },
                    { data: 'origin_airport' , title:"Origin airport"},
                    { data: 'destination_airport' , title:'Destination airport'},
                    { data: 'destination_name' , title:'Destination name', orderable:false },
                    { data: 'departure_date', title:'Departure Date', width: "15%"  },
                    { data: 'departure_dayoftheweek' , title:'Departure day of week', orderable:false },
                    { data: 'return_date', title:'Return Date',width: "15%"  },
                    { data: 'return_dayoftheweek' , title:'Return day of week', orderable:false},
                    { data: 'total_nights' , title:'Total nights', orderable:false  },
                    { data: 'price', title:'Price'  },
                ],
            select: true,
            //search: true,
            layout: {
                    bottomStart: 'pageLength',
                    topStart: {
                        buttons: [
                            { text: '<i class="fas fa-trash-alt"></i> Delete',
                                    className: "btn-danger",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {

                                            var id = sel_row.DT_RowId;
                                            $.ajax({
                                                type: "DELETE",
                                                url: "/del_exported_row/"+id,
                                                traditional: true,
                                                success: function (respuest) {
                                                    table.ajax.reload();
                                                }
                                            });
                                        }
                                    }
                            },
                            { text: '<i class="fas fa-file-csv"></i> Export CSV',
                                    className: "btn-success",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();
                                        //console.log(sel_row)
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {

                                            $("#myModal").modal();
                                        }
                                    }
                            },
                            { text: '<i class="fab fa-internet-explorer"></i> Test URL',
                                    className: "btn-info",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();

                                        var id = sel_row.DT_RowId;
                                        $.ajax({
                                            type: "GET",
                                            url: "/get_export_url_for_test/export/"+id,
                                            traditional: true,
                                            success: function (respuest) {
                                                window.open(respuest, "mywindow", "status=1,toolbar=1");
                                            }
                                        });
                                    }
                            },
                        ]
                   }
            }
            });

        table.buttons().container().appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

        table.on( 'select deselect', function () {
                var selectedRows = table.rows( { selected: true } ).count();
                table.button( 0 ).enable( selectedRows === 1 );
                table.button( 1 ).enable( selectedRows > 0 );
                table.button( 2 ).enable( selectedRows === 1 );
            } );


        $('#confirmDeleteRecord').on('click', function (e) {
            var sel_row= table.row( { selected: true } ).data();
            var id = sel_row.DT_RowId;
            $.ajax({
                type: "DELETE",
                url: "/del_get_flights_search_row/"+id,
                //data: {"id":id},
                traditional: true,
                beforeSend: function () {
                    //$('#msgUsuario').html('<span></span>');
                },
                success: function (respuest) {
                    $('#deleteConfirmModal').modal('hide');
                    table.ajax.reload();;
                }
            });
        });

        $(".modal-footer > .btn-success").on('click', function (e) {
            var file_name = $("#file_name").val();
            var save_new_file = false;
            var selected = new Array();
            $("#Checker input:checkbox:checked").each(function () {
                selected.push(this.value);
            });
            if (selected.length > 0) save_new_file = true;
            var maincsv = $('#maincsvselect input:radio:checked').val();
            var ids = new Array();
            $('#dataTableExportResult tbody tr.selected').each( function() {
                ids.push($(this).attr("id"));
            });
            $.ajax({
                type: "POST",
                url: "/move_data_export",
                data: {"file_name":file_name,
                        'save_new_file':save_new_file,
                        'ids':ids.join("|"),
                        'maincsv':maincsv
                },
                traditional: true,
                success: function (respuest) {
                    $('#myModal').modal('hide');
                    table.ajax.reload();;
                }
            });
        });
  });
