$(document).ready(function() {

    var table =$('#dataTableTasks').DataTable({
                ajax: '/tasks/list/',
                columns: [
                        { data: 'name' , title: "Name"},
                        { data: 'alg_version' ,
                            width: '7%',
                            title: "Algorithm version"
                        },
                        {   data: 'auto' ,
                            title: "Auto execution",
                            width: '7%',
                            className: 'dt-center',
                            render: function ( data, type, row ) {
                                var html = data;
                                if (data == true) {
                                        return '<i class="fas fa-check"></i>';
                                      } else {
                                        return '<i></i>';
                                      }
                            },
                        },
                        { data: 'dt_start' ,
                            title: "Date start"
                        },
                        { data: 'dt_end' ,
                            title: "Date end"},
                        { data: 'percentage' ,
                            title: "Progress"},
                    ],
                select: true,
                bottomStart: 'pageLength',
                layout: {
                    bottomStart: 'pageLength',
                    topStart: {
                        buttons: [{ text: '<i class="fas fa-flag-checkered"></i> Run',
                                    className: "btn-success",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            $.ajax({
                                                    type: "POST",
                                                    url: "/tasks/run/"+sel_row.id,
                                                    traditional: true,
                                                    success: function (respuest) {
                                                        alert("Task was successfully started ");
                                                    },
                                                    error: function(jqXHR, exception, errorThrown){
                                                        console.log(exception);
                                                        console.log(jqXHR);
                                                        console.log(errorThrown);
                                                        alert("Error: "+jqXHR.responseText);
                                                    }
                                                });
                                        }
                                    },
                            },
                            ]
                    },
                }

    });

    table.on( 'select deselect', function () {
        var selectedRows = table.rows( { selected: true } ).count();
        table.button( 0 ).enable( selectedRows === 1 );
        //table.button( 1 ).enable( selectedRows > 0 );
    } );

    setInterval(function(){
        table.ajax.reload(null, false);
    }, 5000);

})

