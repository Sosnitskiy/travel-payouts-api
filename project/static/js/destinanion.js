
$(document).ready(function() {


var table =$('#dataTable1').DataTable({
    processing: false,
    serverSide: true,
    stateSave: true,
    pageLength: 10,
    lengthMenu: [ [10, 25, 50, 100, 300], [10, 25, 50, 100, "300"] ],
    pagingType: "full_numbers",
    order: [
        [1, 'asc'],
    ],
    ajax: '/get_destinations',
    columns: [
            { data: 'name', title: 'Name'},
            { data: 'iata_code', title:"IATA Code" },
            { data: 'v3_enable',
                title:"V3 Scan",
                className: 'dt-center',
                render: function ( data, type, row ) {
                    var html = data;
                    if (data == true) {
                        return '<i class="fas fa-check"></i>';
                    } else {
                        return '';
                   }
                },

             },
        ],
    select: true,
    layout: {
                    bottomStart: 'pageLength',
                    topStart: {
                        buttons: [
                            { text: '<i class="fas fa-trash-alt"></i> Delete',
                                    className: "btn-danger",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var row_data = table.row( { selected: true } ).data();
                                        console.log(row_data)
                                        if (typeof row_data === "undefined"){
                                            alert("Please select row");
                                        } else {
                                            var row_id = row_data.DT_RowId;
                                            $('#deleteConfirmModal > div > div > div.modal-body > input[name="agree"]').val(row_id);
                                            $('#deleteConfirmModal').modal("show");
                                        }
                                    },
                            },
                            { text: '<i class="fas fa-star-half-alt"></i> V3 Enable / Disable',
                                    className: "btn-success",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var row_data = table.row( { selected: true } ).data();
                                        if (typeof row_data === "undefined"){
                                            alert("Please select row");
                                        } else {
                                            var row_id = row_data.DT_RowId;

                                            $.ajax({
                                                type: "POST",
                                                url: "/v3_enable/"+row_id,
                                                //data: {"id":id},
                                                traditional: true,
                                                success: function (respuest) {
                                                    table.ajax.reload();
                                                }
                                            });
                                        }
                                    },
                            },



                                ]
                    }
    }
    });

table.buttons().container()
    .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

table.on( 'select deselect', function () {
        var selectedRows = table.rows( { selected: true } ).count();
        table.button( 0 ).enable( selectedRows === 1 );
        table.button( 1 ).enable( selectedRows > 0 );
    } );


$('#confirmDeleteRecord').on('click', function (e) {
    var id = $('#dataTable1 tbody tr.selected').attr("id");
    $.ajax({
        type: "DELETE",
        url: "/del_destination/"+id,
        //data: {"id":id},
        traditional: true,
        beforeSend: function () {
            //$('#msgUsuario').html('<span></span>');
        },
        success: function (respuest) {
            $('#deleteConfirmModal').modal('hide');
            $('#dataTable1').DataTable().ajax.reload();
        }
    });
});
});
