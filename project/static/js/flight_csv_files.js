
  $(document).ready(function(){

         var table =$('#dataTableCSVFileList').DataTable({
            processing: false,
            serverSide: true,
            stateSave: true,
            pageLength: 10,
            lengthMenu: [ [10, 25, 50, 100, 300], [10, 25, 50, 100, "300"] ],
            pagingType: "full_numbers",
            ajax: '/get_csv_files_list',
            columns: [
                    { data: 'name' },
                ],
            select: true,
            search: {
                    return: true,
                },
            layout: {
                    bottomStart: 'pageLength',
                    topStart: {
                        buttons: [
                            { text: '<i class="fas fa-trash-alt"></i> Delete',
                                    className: "btn-danger",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();

                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            $.ajax({
                                                type: "DELETE",
                                                url: "/del_csv_files_list/"+sel_row.DT_RowId,
                                                traditional: true,
                                                success: function (respuest) {
                                                    table.ajax.reload();
                                                    tableChild.ajax.reload();
                                                }
                                            })
                                        }
                                    }
                            },
                            { text: '<i class="fas fa-download"></i> Download',
                                    className: "btn-success",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            var name_file = sel_row.name;
                                            window.open("/files/"+name_file);
                                        }
                                    }
                            }
                        ]
                        }
            },

            });

        table.buttons().container().appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

        table.on( 'select deselect', function () {
                var selectedRows = table.rows( { selected: true } );
                var id=0;
                var file_name = ""
                var message = "";
                if (selectedRows.data().length > 0){
                    id = selectedRows.data()[0].DT_RowId;
                    file_name = selectedRows.data()[0].name;
                }

                table.button( 0 ).enable( selectedRows.count() === 1 );
                table.button( 1 ).enable( selectedRows.count()  === 1);
                if (parseInt(id) < 2) {
                    message = "EDIT FILE - "+file_name + " (main file)";
                } else {
                    message = "EDIT FILE - "+file_name;
                }
                $('#markreadedfile').text("EDIT FILE - "+file_name);
                tableChild.ajax.reload();
            } );


        $('#confirmDeleteRecord').on('click', function (e) {
            var id = $('#dataTableExportResult tbody tr.selected').attr("id");
            $.ajax({
                type: "DELETE",
                url: "/del_get_flights_search_row/"+id,
                //data: {"id":id},
                traditional: true,
                beforeSend: function () {
                    //$('#msgUsuario').html('<span></span>');
                },
                success: function (respuest) {
                    $('#deleteConfirmModal').modal('hide');
                    $('#dataTableExportResult').DataTable().ajax.reload();
                }
            });
        });



        var tableChild =$('#dataTableCSVFileInternal').DataTable({
            processing: false,
            serverSide: true,
            stateSave: true,
            pageLength: 10,
            lengthMenu: [ [10, 25, 50, 100, 300], [10, 25, 50, 100, "300"] ],
            pagingType: "full_numbers",
            //info: true,
            //autoWidth: false,
            ajax: {
                "url": "/get_csv_files_internal",
                "type": "POST",
                "data": function ( d ) {
                    var sel_row = table.rows( { selected: true } ).data();
                    if (sel_row.data().length > 0){
                        console.log(sel_row);
                        if (sel_row.length === 0){
                            d.ParentId = 0;
                        } else {
                            d.ParentId = sel_row[0].DT_RowId;
                        }
                    }
                }},
            columns: [
                    { data: 'name' , title:"Name", width: "25%", orderable:false },
                    { data: 'origin_airport' , title:"Origin airport"},
                    { data: 'destination_airport' , title:'Destination airport'},
                    { data: 'destination_name' , title:'Destination name', orderable:false},
                    { data: 'departure_date' , title:'Departure Date', width: "15%"},
                    { data: 'departure_dayoftheweek' , title:'Departure day of week', orderable:false },
                    { data: 'return_date' , title:'Return Date',width: "15%" },
                    { data: 'return_dayoftheweek' , title:'Return day of week', orderable:false},
                    { data: 'total_nights' , title:'Total nights', orderable:false },
                    { data: 'price' , title:'Price' },
                ],
            select: true,
            search: true,
            layout: {
                    bottomStart: 'pageLength',
                    topStart: {
                        buttons: [
                            { text: '<i class="fas fa-trash-alt"></i> Delete',
                                    className: "btn-danger",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= tableChild.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            $.ajax({
                                                type: "DELETE",
                                                url: "/del_deleterow2/"+sel_row.DT_RowId,
                                                traditional: true,
                                                success: function (respuest) {
                                                    tableChild.ajax.reload();
                                                }
                                            });
                                        }
                                    }
                            },
                            { text: '<i class="fab fa-internet-explorer"></i> Test URL',
                                    className: "btn-info",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= tableChild.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            $.ajax({
                                                type: "GET",
                                                url: "/get_export_url_for_test/files/"+sel_row.DT_RowId,
                                                traditional: true,
                                                success: function (respuest) {
                                                    window.open(respuest, "mywindow", "status=1,toolbar=1");
                                                }
                                            });


                                        }
                                    }
                            },

                        ]
                    }
            }
            });

        tableChild.buttons().container().appendTo( $('.col-sm-6:eq(0)', tableChild.table().container() ) );

        tableChild.on( 'select deselect', function () {
                var selectedRows = tableChild.rows( { selected: true } ).count();
                tableChild.button( 0 ).enable( selectedRows === 1);
                tableChild.button( 1 ).enable( selectedRows === 1);
                //console.log(selectedRows);
            } );
  });
