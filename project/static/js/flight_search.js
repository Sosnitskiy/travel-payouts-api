
  $(document).ready(function(){
    var dtp1_start = $("#store_datepicker1_from").val();
    var dtp1_end = $("#store_datepicker1_to").val();
    var dtp2_start = $("#store_datepicker2_from").val();
    var dtp2_end = $("#store_datepicker2_to").val();
    $('.datepicker').tDatePicker({  dateCheckIn: dtp1_start,
                                    dateCheckOut: dtp1_end,
                                    limitNextMonth: 300,
                                    limitDateRanges : 6000,
    });
    $('[data-toggle="popover"]').popover();


$.fn.dataTable.ext.buttons.test_url = {
            text: 'Test URL',
            enabled: false,
            action: function ( e, dt, node, config ) {

                var id = $('#dataTableSearchResult tbody tr.selected').attr("id");
                $.ajax({
                    type: "GET",
                    url: "/get_export_url_for_test/search/"+id,
                    traditional: true,
                    beforeSend: function () {
                        //$('#msgUsuario').html('<span></span>');
                    },
                    success: function (respuest) {
                        window.open(respuest, "mywindow", "status=1,toolbar=1");
                    }
                });
            }
        };
var table =$('#dataTableSearchResult').DataTable({
    //dom: "Bfrtip",
    processing: false,
    serverSide: true,
    stateSave: true,
    //paging: true,
    pageLength: 10,
    lengthMenu: [ [10, 25, 50, 100, 300], [10, 25, 50, 100, "300"] ],
    pagingType: "full_numbers",
    //info: true,
    autoWidth: false,
    //deferRender: true,
    ajax: '/get_flights_search_result',
    columns: [
            { data: 'name', title:"Name", width: "25%", orderable:false },
            { data: 'origin_airport', title:"Origin airport"},
            { data: 'destination_airport', title:'Destination airport'},
            { data: 'destination_name', title:'Destination name', orderable:false  },
            { data: 'departure_date', title:'Departure Date', width: "15%" },
            { data: 'departure_dayoftheweek', title:'Departure day of week', orderable:false  },
            { data: 'return_date', title:'Return Date',width: "15%" },
            { data: 'return_dayoftheweek' , title:'Return day of week', orderable:false },
            { data: 'total_nights', title:'Total nights', orderable:false  },
            { data: 'price', title:'Price' },
        ],
    select: true,
    search: {
            return: true,
        },
    layout: {
                    bottomStart: 'pageLength',
                    topStart: {
                        buttons: [
                            { text: '<i class="fas fa-trash-alt"></i> Delete',
                                    className: "btn-danger",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            $.ajax({
                                                type: "DELETE",
                                                url: "/del_get_flights_search_row/"+sel_row.DT_RowId,
                                                traditional: true,
                                                success: function (respuest) {
                                                    table.ajax.reload();
                                                }
                                            });
                                        }
                                    }
                            },
                            { text: '<i class="fas fa-file-export"></i> Add Selected to Export',
                                    className: "btn-success",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.rows( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            var ids = [];
                                            var tt=0;
                                            $.each(sel_row, function(indx, val) {
                                                ids.push(val.DT_RowId);
                                            });
                                            $.ajax({
                                                type: "POST",
                                                url: "/add_flights_search_result/"+ids.join("|"),
                                                traditional: true,
                                                success: function (reqpuest) {
                                                    if(reqpuest.skipped > 0){
                                                        $('.skipped').text(reqpuest.skipped +' Items skipped as duplicates');
                                                    } else {
                                                        $('.skipped').text('');
                                                    }
                                                    $('.addedrows').text(reqpuest.added +' Items added to current export');

                                                }
                                            });
                                        }
                                    }
                            },
                            { text: '<i class="fab fa-internet-explorer"></i> Test URL',
                                    className: "btn-info",
                                    enabled: false,
                                    action: function(e, dt, node, config) {
                                        var sel_row= table.row( { selected: true } ).data();
                                        if (typeof sel_row === "undefined"){
                                            alert("please select row");
                                        } else {
                                            $.ajax({
                                                type: "GET",
                                                url: "/get_export_url_for_test/search/"+sel_row.DT_RowId,
                                                traditional: true,
                                                success: function (respuest) {
                                                    window.open(respuest, "mywindow", "status=1,toolbar=1");
                                                }
                                            });


                                        }
                                    }
                            },
                        ]
                    }
    },

    });

//table.buttons().container()
//    .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );

table.on( 'select deselect', function () {
        var selectedRows = table.rows( { selected: true } ).count();
        table.button( 0 ).enable( selectedRows === 1 );
        table.button( 1 ).enable( selectedRows > 0 );
        table.button( 2 ).enable( selectedRows === 1 );
    } );


$('#confirmDeleteRecord').on('click', function (e) {
    var id = $('#dataTableSearchResult tbody tr.selected').attr("id");
    $.ajax({
        type: "DELETE",
        url: "/del_get_flights_search_row/"+id,
        //data: {"id":id},
        traditional: true,
        beforeSend: function () {
            //$('#msgUsuario').html('<span></span>');
        },
        success: function (respuest) {
            $('#deleteConfirmModal').modal('hide');
            $('#dataTableSearchResult').DataTable().ajax.reload();;
        }
    });
});


var form =$('#flight_search_second').submit(function () {
        var destination_airport = $("#destination_airport").val().split(" | ")[1];
        var origin_airport = $("#origin_airport").val().split(" | ")[1];
        $("<input>").attr({'type':'hidden','name':'destination_airport'}).val(destination_airport).appendTo(form);
        $("<input>").attr({'type':'hidden','name':'origin_airport'}).val(origin_airport).appendTo(form);
});




  });
