from project import db
from sqlalchemy import UniqueConstraint
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from sqlalchemy import exc


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(100))
    active = db.Column(db.Boolean())
    authenticated = db.Column(db.Boolean())

    # Flask-Login User Methods

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)  # Python 3

    def __repr__(self):
        return "<User %r>" % (self.username)


class Weekdays(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    hebr_day = db.Column(db.String(10))
    __table_args__ = (
        UniqueConstraint("name"),
    )


class Destinations(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    iata_code = db.Column(db.String(10))
    v3_enable = db.Column(db.Boolean(), default=False)
    __table_args__ = (
        UniqueConstraint("iata_code"),
    )


class Request(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at=db.Column(db.DateTime(timezone=True), server_default=func.now())
    origin_airport = db.Column(db.String(3))
    destination_airport = db.Column(db.String(3))
    t_start = db.Column(db.Date())
    t_end = db.Column(db.Date())
    max_price = db.Column(db.Integer())
    transfers = db.Column(db.Integer())
    return_transfers = db.Column(db.Integer())
    min_nights_duration = db.Column(db.Integer())
    max_nights_duration = db.Column(db.Integer())
    responces = relationship("Response")


class Response(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String(3))
    destination = db.Column(db.String(3))
    origin_airport = db.Column(db.String(3))
    destination_airport = db.Column(db.String(3))
    price = db.Column(db.Integer())
    airline = db.Column(db.String(3))
    flight_number = db.Column(db.String(30))
    departure_at = db.Column(db.DateTime())
    return_at = db.Column(db.DateTime())
    transfers = db.Column(db.Integer())
    return_transfers = db.Column(db.Integer())
    duration = db.Column(db.Integer())
    link = db.Column(db.String(500))
    currency = db.Column(db.String(3))
    request_id = db.Column(db.Integer, ForeignKey("request.id"))


class File_csv(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    name = db.Column(db.String(100))
    contextcsvs = relationship("Context_csv", cascade="all,delete", backref="parent")


class Context_csv(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    origin = db.Column(db.String(3))
    destination = db.Column(db.String(3))
    origin_airport = db.Column(db.String(3))
    destination_airport = db.Column(db.String(3))
    price = db.Column(db.Integer())
    airline = db.Column(db.String(3))
    flight_number = db.Column(db.String(30))
    departure_at = db.Column(db.DateTime())
    return_at = db.Column(db.DateTime())
    transfers = db.Column(db.Integer())
    return_transfers = db.Column(db.Integer())
    duration = db.Column(db.Integer())
    link = db.Column(db.String(500))
    currency = db.Column(db.String(3))
    direct = db.Column(db.Boolean())
    key_unic = db.Column(db.String(100))
    file_id = db.Column(db.Integer, ForeignKey("file_csv.id"))
    __table_args__ = (
        UniqueConstraint("key_unic", "file_id"),
    )
