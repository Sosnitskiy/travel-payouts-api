from project import celery
from celery import group, chain, chord
from celery import current_app
from celery.schedules import crontab
from flask import Blueprint, render_template, Response
from flask_login import login_required
from project import redis_connect
from project.flight_tickets_api import AviasalesData
from project.models import Context_csv, Destinations
from project.custom_errors import ToManyRetry
import datetime
import logging
from logging.handlers import RotatingFileHandler
import psycopg2
from psycopg2.errors import UniqueViolation
from pytz import timezone
from project.utilites import name_generator, key_generator, get_dey_of_week, key_generator_v3
import email, smtplib, ssl
import pytz
import os

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText




task_worker = Blueprint('task_worker', __name__)

logger = logging.getLogger('my_logger')
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler('task.log', maxBytes=100000,backupCount=10)
logger.addHandler(handler)

@task_worker.context_processor
def additional_context():
    def count_buffer():
        return Context_csv.query.filter_by(file_id=0).count()
    return dict(count_buffer=count_buffer)

@login_required
@task_worker.route('/tasks/', methods=["GET"])
def get_task_templ():
    return render_template('tasks.html')

def generate_task_list(id=None):
    result = list()
    tasks_list = [
        {"id": 1,
         "tech_name": "week15_200baks",
         "alg_version": "v1",
         "max_cost": 200,
         "weeks": 15,
         "cron_run": None,
         #"cron_run": [{"minute": '45',"hour":'12' }, {"minute": '22',"hour":'14' },{"minute": '32',"hour":'14' },],
         "name": "all flights auto daily 15 weeks 200 usd",
         "file_name": "all_flights_auto_daily_15weeks_200usd.csv"
         },
        {"id": 2,
         "tech_name": "week15_500baks",
         "alg_version": "v1",
         "max_cost": 500,
         "weeks": 15,
         "cron_run": None,
         "name": "all flights auto daily 15 weeks 500 usd",
         "file_name": "all_flights_auto_daily_15weeks_500usd.csv"
         },
        {"id": 3,
         "tech_name": "week15_1200baks",
         "alg_version": "v1",
         "max_cost": 1200,
         "weeks": 15,
         "cron_run": None,
         "name": "all flights auto daily 15 weeks 1200 usd",
         "file_name": "all_flights_auto_daily_15weeks_1200usd.csv"
         },
        {"id": 4,
         "tech_name": "week15_2000baks",
         "alg_version": "v1",
         "max_cost": 2000,
         "weeks": 15,
         "cron_run": None,
         "name": "all flights auto daily 15 weeks 2000 usd",
         "file_name": "all_flights_auto_daily_15weeks_2000usd.csv"
         },
        {"id": 5,
         "tech_name": "week30_2000baks",
         "alg_version": "v1",
         "max_cost": 2000,
         "weeks": 30,
         "cron_run": None,
         "name": "all flights auto daily 30 weeks 2000 usd",
         "file_name": "all_flights_auto_daily_30weeks_2000usd.csv"
         },
        {"id": 6,
         "tech_name": "week30_2000baks_v2",
         "alg_version": "v2",
         "max_cost": 2000,
         "weeks": 30,
         "cron_run": None,
         "name": "all flights auto daily 30 weeks 2000 usd V2",
         "file_name": "all_flights_auto_daily_30weeks_2000usd_v2.csv"
         },
        {"id": 7,
         "tech_name": "week15_2000baks_v2",
         "alg_version": "v2",
         "max_cost": 2000,
         "weeks": 15,
         "cron_run": None,
         "name": "all flights auto daily 15 weeks 2000 usd V2",
         "file_name": "all_flights_auto_daily_15weeks_2000usd_v2.csv"
         },
        {"id": 8,
         "tech_name": "week52_5000baks_v2",
         "alg_version": "v2",
         "max_cost": 5000,
         "weeks": 52,
         #"cron_run": None,
         "cron_run": [{"minute": '0', "hour": '0'},],
         "name": "all flights auto daily 52 weeks 5000 usd V2",
         "file_name": "all_flights_auto_daily_52weeks_5000usd_v2.csv"
         },
        {"id": 9,
         "tech_name": "week52_2000baks_v2",
         "alg_version": "v2",
         "max_cost": 2000,
         "weeks": 52,
         "cron_run": None,
         "name": "all flights auto daily 52 weeks 2000 usd V2",
         "file_name": "all_flights_auto_daily_52weeks_2000usd_v2.csv"
         },


        {"id": 10,
         "tech_name": "week52_2000baks_v3",
         "alg_version": "v3",
         "max_cost": 2000,
         "weeks": 52,
         "cron_run": None,
         "name": "all flights auto daily 52 weeks 2000 usd V3",
         "file_name": "all_flights_auto_daily_52weeks_2000usd_v3.csv"
         },
        {"id": 11,
         "tech_name": "week26_2000baks_v3",
         "alg_version": "v3",
         "max_cost": 2000,
         "weeks": 26,
         # "weeks": 1,
         #"cron_run": None,
          "cron_run": [{"minute": '0', "hour": '2'}, ],
         "name": "all flights auto daily 26 weeks 2000 usd V3",
         "file_name": "all_flights_auto_daily_26weeks_2000usd_v3.csv"
         },
        {"id": 12,
         "tech_name": "week52_5000baks_v3",
         "alg_version": "v3",
         "max_cost": 5000,
         "weeks": 52,
         # "weeks": 1,
         "cron_run":None,
         "name": "all flights auto daily 52 weeks 5000 usd V3",
         "file_name": "all_flights_auto_daily_52weeks_5000usd_v3.csv"
         },
        {"id": 13,
         "tech_name": "week26_5000baks_v3",
         "alg_version": "v3",
         "max_cost": 5000,
         "weeks": 26,
         "cron_run": None,
         "name": "all flights auto daily 26 weeks 5000 usd V3",
         "file_name": "all_flights_auto_daily_26weeks_5000usd_v3.csv"
         },
    ]
    if id:
        for row in tasks_list:
            if row["id"] == id:
                result = row.copy()
        if not result:
            raise Exception(f"task with id '{id}' nor defined in function generate_task_list")
    else:
        result = tasks_list.copy()
    return result

@login_required
@task_worker.route('/tasks/list/', methods=["GET"])
def get_task_list():
    tasks_list = generate_task_list()
    data_list = list()
    for task in tasks_list:
        key_ = "tasks:{0}:start".format(task["tech_name"])
        dt_start = redis_connect.get(key_)

        key_ = "tasks:{0}:end".format(task["tech_name"])
        dt_end = redis_connect.get(key_)

        key_ = "tasks:{0}:finished_subtask".format(task["tech_name"])
        part_tasks = redis_connect.get(key_)

        key_ = "tasks:{0}:total_subtask".format(task["tech_name"])
        total = redis_connect.get(key_)

        part_tasks_int = part_tasks.decode("utf-8") if part_tasks else 0
        total_int = total.decode("utf-8") if total else 0

        if float(total_int):
            percent = 100 * (float(part_tasks_int) / float(total_int))
        else:
            percent = 0
        percentage = "{0}/{1} ( {2}% )".format(total_int, part_tasks_int, round(percent, 2))
        data_list .append({"name": task["name"],
                           "dt_start": dt_start.decode("utf-8") if dt_start else "",
                           "dt_end": dt_end.decode("utf-8") if dt_end else "",
                           "auto": True if task.get("cron_run", None) else False,
                           "alg_version":  task["alg_version"],
                           "cron_run": task["cron_run"] if task.get("cron_run", None) else "",
                           "percentage": percentage,
                           "id": task["id"]
                           })
    recTotal = len(data_list)
    result = dict(data=data_list,
                  recordsTotal=recTotal,#=count_rec,
                  recordsFiltered=recTotal,#=data_rows.total,
                  current_page=1,#=data_rows.page,
                  per_page=10#=data_rows.per_page,
                  )
    return result

@login_required
@task_worker.route('/tasks/run/<int:id>', methods=["POST"])
def run_task(id):
    result = "success"
    try:
        task_def = generate_task_list(id)
        #key = "tasks:{0}:status".format(task_def["tech_name"])
        #status = redis_connect.get(key)
        #if status and status==b"START":
        #    raise Exception("Task is running")

        if task_def.get("alg_version") == "v1":
            week15_xxx_baks.delay(id)
        elif task_def.get("alg_version") == "v2":
            week15_xxx_baks.delay(id)
        elif task_def.get("alg_version") == "v3":
            week15_xxx_baks.delay(id)
        elif task_def.get("alg_version") == "v4":
            week15_xxx_baks.delay(id)
        else:
            raise Exception("Algorithm version not implemented yet")
    except Exception as ex:
        return Response(str(ex), 422)
    return result

def fill_by_excel(templ):
    result = list()
    days_list = list()
    days_list.append(('Sunday','Thursday'))
    days_list.append(('Thursday','Sunday'))
    days_list.append(('Monday','Friday'))
    days_list.append(('Monday','Thursday'))
    days_list.append(('Tuesday','Saturday'))
    days_list.append(('Tuesday','Sunday'))
    days_list.append(('Monday','Sunday'))
    days_list.append(('Sunday','Sunday'))
    days_list.append(('Monday','Monday'))
    days_list.append(('Tuesday','Tuesday'))
    days_list.append(('Wednesday','Wednesday'))
    days_list.append(('Thursday','Thursday'))
    days_list.append(('Friday','Friday'))
    days_list.append(('Saturday','Saturday'))

    for day_data in days_list:
        res = templ.copy()
        res['start_day'] = day_data[0]
        res['end_day'] = day_data[1]
        result.append(res)
    return result

def loop_find_end_date(start_dt, day_of_week):
    delt = 1
    result = None
    weekDays_ = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    while True:
        date_verif = start_dt + datetime.timedelta(days=delt)
        delt += 1
        if weekDays_[date_verif.weekday()] == day_of_week:
            result = date_verif
            break
        if delt > 1000:
            raise Exception ("!!!! Owerloop !!!!")

    return result

def fill_dates(days_list, weeks):
    result = list()
    today = datetime.datetime.today()
    weekDays_il = ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
    for week in range(weeks):
        logger.info(" Week {} :".format(week))
        if week == 0:
            tomorow = today + datetime.timedelta(days=1)
            week_d1 = tomorow.weekday() + 1
            weekDays = weekDays_il[week_d1:]
        else:
            weekDays = weekDays_il
            tomorow = today + datetime.timedelta(weeks=week)
            tomorow = tomorow - datetime.timedelta(days=tomorow.weekday() + 1)

        for rule in days_list:
            start_day = rule.get("start_day")
            if start_day in weekDays:
                num_st = weekDays.index(start_day)
                rule['departure_at'] = tomorow + datetime.timedelta(days=num_st)
                rule['return_at'] = loop_find_end_date(rule['departure_at'], rule['end_day'])
            logger.info("sd: {0}, sd_dt: {1}, ed:{2}, end_dt: {3}".format(rule['start_day'], rule["departure_at"], rule['end_day'],rule['return_at']))
        for rule in days_list:
            if rule['departure_at']:
                rule['departure_at'] = rule['departure_at'].strftime('%Y-%m-%d')
                rule['return_at'] = rule['return_at'].strftime('%Y-%m-%d')
                result.append(rule.copy())

    return result

def fill_dates_v2(templ, week_to_future):
    result = list()
    days_to_future = week_to_future * 7
    for day_current in range(days_to_future):
        #print(f" --- day {day_current} --- ")
        today = datetime.datetime.now() + datetime.timedelta(days=day_current +1)
        for day_data in range(13):
            nxt_day = today + datetime.timedelta(days=day_data+2)
            res = templ.copy()
            res['start_day'] = today.strftime('%A')
            res['end_day'] = nxt_day.strftime('%A')
            res['departure_at'] = today.strftime('%Y-%m-%d')
            res['return_at'] = nxt_day.strftime('%Y-%m-%d')
            result.append(res)
            #print("{0} {1}-{2} {3}".format(res['departure_at'], res['start_day'], res['return_at'], res['end_day']))

    return result

def fill_dates_v3(connection, templ, week_to_future):
    result = list()
    days_to_future = week_to_future * 7

    cursor = connection.cursor()
    str_sql = "select d.iata_code from destinations d where d.v3_enable = true;"
    cursor.execute(str_sql)

    all_destinationn = set()
    for row in cursor.fetchall():
        all_destinationn.add(row[0])

    for day_current in range(days_to_future):
        #print(f" --- day {day_current} --- ")
        today = datetime.datetime.now() + datetime.timedelta(days=day_current +1)
        for day_data in range(13):
            nxt_day = today + datetime.timedelta(days=day_data+2)
            for direct in ("true", "false"):
                if all_destinationn:
                    for dest_ in all_destinationn:
                        res = templ.copy()
                        if res['origin'] == dest_:
                            print("SKIP WRONG DESTINATION: {0}".format(dest_))
                        else:

                            res['start_day'] = today.strftime('%A')
                            res['end_day'] = nxt_day.strftime('%A')
                            res['departure_at'] = today.strftime('%Y-%m-%d')
                            res['return_at'] = nxt_day.strftime('%Y-%m-%d')
                            res['direct'] = direct
                            res['destination'] = dest_
                            del (res['transfers'])
                            del (res['return_transfers'])
                            result.append(res)
                else:
                    res = templ.copy()
                    res['start_day'] = today.strftime('%A')
                    res['end_day'] = nxt_day.strftime('%A')
                    res['departure_at'] = today.strftime('%Y-%m-%d')
                    res['return_at'] = nxt_day.strftime('%Y-%m-%d')
                    res['direct'] = direct
                    del (res['transfers'])
                    del (res['return_transfers'])
                    result.append(res)

    return result

def fill_dates_v4(templ, week_to_future):
    result = list()
    days_to_future = week_to_future * 7
    for day_current in range(days_to_future):
        #print(f" --- day {day_current} --- ")
        today = datetime.datetime.now() + datetime.timedelta(days=day_current +1)
        for day_data in range(13):
            nxt_day = today + datetime.timedelta(days=day_data+2)
            for direct in ("true", "false"):
                res = templ.copy()
                res['start_day'] = today.strftime('%A')
                res['end_day'] = nxt_day.strftime('%A')
                res['departure_at'] = today.strftime('%Y-%m-%d')
                res['return_at'] = nxt_day.strftime('%Y-%m-%d')
                res['direct'] = direct
                res['destination'] = "JFK"
                del(res['transfers'])
                del(res['return_transfers'])
                result.append(res)
            #print("{0} {1}-{2} {3}".format(res['departure_at'], res['start_day'], res['return_at'], res['end_day']))

    return result

@celery.task
def test11(*args, **kwargs):
    logger.info("YA STARTED")
    print("sddsdsdsdsdsds")


class BaseTaskWithRetry(celery.Task):
    autoretry_for = (ToManyRetry,)
    max_retries = 5
    retry_backoff = False
    retry_backoff_max = 300
    retry_jitter = True
    def on_retry(self, exc, task_id, args, kwargs, einfo):
        dd = 0
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        task_def = args[1]
        redis_key_pref = task_def['tech_name']
        redis_connect.incr(f"tasks:{redis_key_pref}:finished_subtask")
    def on_success(self, retval, task_id, args, kwargs):
        task_def = args[1]
        redis_key_pref = task_def['tech_name']
        redis_connect.incr(f"tasks:{redis_key_pref}:finished_subtask")


    #, rate_limit='1580/m'
@celery.task(bind=True, base=BaseTaskWithRetry, queue='celery', rate_limit='1600/m')
def execute_request(self,file_id,  task_def, kwargs):
    #print("1")
    redis_key_pref = task_def['tech_name']
    #logger.info("Create connection")
    connection = psycopg2.connect(user="user2",
                                  password="XmLuwoRTaPBB",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="travel")
    #print("2")
    try:
        # Create a cursor to perform database operations
        #logger.info("Create cursor")
        cursor = connection.cursor()


        #logger.info("insert initial data to class AviasalesData")
        req_cls = AviasalesData(currency='USD',
                                origin=kwargs.get("origin", None),
                                destination=kwargs.get("destination", None),
                                departure_at=kwargs.get("departure_at", None),
                                return_at=kwargs.get("return_at", None),
                                direct=kwargs.get("direct", None),
                                limit=200,
                                page=1,
                                sorting="price",
                                unique=False,
                                req=kwargs)
        #print("3")
        #logger.info("   create request")
        req_cls.create_request()
        #print("4")
        #logger.info("   parse returned rows")
        for row in req_cls.data:
            #logger.info("   validates record")
            if kwargs.get("direct", None):
                # for v3 algoritm
                direct_key = True
                if row.get("transfers", None) or row.get("return_transfers", None):
                    direct_key = False
                row["direct"] = direct_key

            extend_validation = req_cls.ext_validate_data(row, req_cls.req)
            if extend_validation:
                str_sql = 'INSERT INTO public.context_csv (origin, destination, origin_airport, destination_airport,'
                str_sql += ' price, airline, flight_number, departure_at, return_at, transfers,'
                str_sql += ' return_transfers, duration, direct, link, currency, key_unic, file_id)'
                str_sql += " VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s);"

                #logger.info("     SQL: {}".format(str_sql))
                data_list = _resp_to_list(row, file_id, alg_version=task_def["alg_version"])
                #logger.info("     data: {}".format(data_list))

                #str_sql = str_sql % data_list

                try:
                    cursor.execute(str_sql, data_list)
                except UniqueViolation as ex:
                    logger.info("record is already exist in DB")
                    connection.rollback()
                except Exception as ex:
                    logger.info("     SQL with data: {}".format(str_sql))
                    logger.exception("Error")

            dd = 0
        #print("5")
        connection.commit()
        #print("6")
    except Exception as error:

        logger.exception('Error:')
    finally:
        #redis_connect.incr(f"tasks:{redis_key_pref}:finished_subtask")
        if (connection):
            cursor.close()
            connection.close()

def _resp_to_list(data, file_id, alg_version="v1"):
    res = list()
    res.append(data.get("origin", None))
    res.append(data.get("destination", None))
    res.append(data.get("origin_airport", None))
    res.append(data.get("destination_airport", None))
    res.append(data.get("price", None))
    res.append(data.get("airline", None))
    res.append(data.get("flight_number", None))
    res.append(data.get("departure_at", None))
    res.append(data.get("return_at", None))
    res.append(data.get("transfers", None))
    res.append(data.get("return_transfers", None))
    res.append(data.get("duration", None))
    res.append(data.get("direct", None))
    res.append('https://www.aviasales.com' + data.get("link", None))
    res.append('USD')
    if alg_version in ("v1", "v2"):
        res.append(key_generator(data.get("origin_airport"),
                                 data.get("destination_airport"),
                                 datetime.datetime.strptime(data.get("departure_at"), "%Y-%m-%dT%H:%M:%S%z"),
                                 datetime.datetime.strptime(data.get("return_at"), "%Y-%m-%dT%H:%M:%S%z"),
                                 data.get("price")
                                 ),
                   )
    elif alg_version in ("v3","v4"):
        direct_key = "DIRECT"
        if data.get("transfers", None) or data.get("return_transfers", None):
            direct_key = "NONE_DIRECT"

        res.append(key_generator_v3( direct = direct_key,
                                   origin_airport = data.get("origin_airport"),
                                   destination_airport = data.get("destination_airport"),
                                   departure_date = datetime.datetime.strptime(data.get("departure_at"), "%Y-%m-%dT%H:%M:%S%z"),
                                   return_date = datetime.datetime.strptime(data.get("return_at"), "%Y-%m-%dT%H:%M:%S%z"),
                                   price = data.get("price"),
                                     transfers= data.get("transfers", None),
                                     return_transfers=data.get("return_transfers", None)

                                 ),
                   )
    res.append(file_id)
    return tuple(res)


@celery.task(queue='celery')
def week15_xxx_baks(row_id:int, *args, **kwargs):
    today = datetime.datetime.today()
    task_def = generate_task_list(row_id)
    week_to_future = int(task_def.get('weeks', 15))
    connection = psycopg2.connect(user="user2",
                                  password="XmLuwoRTaPBB",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="travel")

    redis_connect.set(f"tasks:{task_def['tech_name']}:status", "START")
    redis_connect.set(f"tasks:{task_def['tech_name']}:error_mess", "")
    redis_connect.set(f"tasks:{task_def['tech_name']}:total_subtask", 0)
    redis_connect.set(f"tasks:{task_def['tech_name']}:finished_subtask", 0)
    redis_connect.set(f"tasks:{task_def['tech_name']}:start", today.strftime("%Y-%m-%d %H:%M:%S"))
    redis_connect.set(f"tasks:{task_def['tech_name']}:end", "")
    tomorow = today + datetime.timedelta(days=1)
    weekDays = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    dd = weekDays[tomorow.weekday()]
    templ = {   'origin': 'TLV',
                'departure_at': None,
                'return_at': None,
                'max_price': task_def['max_cost'],
                'transfers': 0,
                'return_transfers': 0,
                'min_nights_duration': 1,
                'max_nights_duration': 30,
                'start_day': None,
                'end_day':None
    }
    logger.info("    Algorithm : {0}".format(task_def["alg_version"]))
    if task_def["alg_version"] == "v1":
        exc_par = fill_by_excel(templ)
        filled_task = fill_dates(exc_par, week_to_future)
    elif task_def["alg_version"] == "v2":
        filled_task = fill_dates_v2(templ, week_to_future)
    elif task_def["alg_version"] == "v3":
        filled_task = fill_dates_v3(connection, templ, week_to_future)
    elif task_def["alg_version"] == "v4":
        filled_task = fill_dates_v4(templ, week_to_future)
    else:
        raise Exception("algorithm version not implemented in code")

    logger.info("       ----------- {} -------------")

    logger.info("Create connection")



    try:
        # Create a cursor to perform database operations
        #logger.info("Create cursor")
        cursor = connection.cursor()
        logger.info(f"find file '{task_def['file_name']}'")
        sql_q = "SELECT id FROM public.file_csv where name ='{0}'".format(task_def['file_name'])
        cursor.execute(sql_q)
        record = cursor.fetchone()
        if record:
            file_id = record[0]
        logger.info("file searched with id={}".format(record))
        if cursor.rowcount == 0:
            logger.info(f"find '{task_def['file_name']}' not founded. Create it")
            cursor.execute(
                f"INSERT INTO public.file_csv (name) VALUES ('{task_def['file_name']}')")
            connection.commit()
            cursor.execute('SELECT LASTVAL();')
            record = cursor.fetchone()
            if record:
                file_id = record[0]

        logger.info("clear list data in context_csv")
        sql_req = 'delete from public.context_csv where file_id={};'.format(file_id)
        cursor.execute(sql_req)
        connection.commit()

        redis_connect.set(f"tasks:{task_def['tech_name']}:total_subtask", len(filled_task))
        group_t = group(execute_request.s(file_id, task_def, kwargs=task_param) for task_param in filled_task)
        result = chord(group_t)(send_mail_by_task1.s(**task_def))

        logger.info("----------------------------------------------------------------------")
    except Exception as ex:
        redis_connect.set(f"tasks:{task_def['tech_name']}:status", "ERROR")
        redis_connect.set(f"tasks:{task_def['tech_name']}:error_mess", str(ex))
        raise ex
    finally:
        if connection:
            cursor.close()
            connection.close()

@celery.task(queue='low_level')
def send_mail_by_task1(*args, **kwargs):
    task_def = kwargs
    logger.info("_____________      mail   ________________________")

    connection = psycopg2.connect(user="user2",
                                  password="XmLuwoRTaPBB",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="travel")

    try:
        # Create a cursor to perform database operations
        file_id = ""
        logger.info("Create cursor mail")
        cursor = connection.cursor()
        cursor.execute("SELECT id FROM public.file_csv where name ='{0}'".format(task_def['file_name']))
        record = cursor.fetchone()
        if record:
            file_id = record[0]
        cursor.execute('select id,origin,destination,origin_airport,destination_airport,price,airline,flight_number,departure_at,return_at,transfers,return_transfers,duration,link,currency,direct from public.context_csv where file_id={}'.format(file_id))
        records = cursor.fetchall()
        count_rec = cursor.rowcount
        column_names = [desc[0] for desc in cursor.description]
        export_rows = list(map(lambda x: dict(zip(column_names, x)), records))

        sql_query = "SELECT id, name, hebr_day FROM public.weekdays"
        cursor.execute(sql_query)
        records = cursor.fetchall()
        hebr_days = dict()
        for row in records:
            hebr_days[row[1]] = row[2]

        sql_query = "SELECT id, name, iata_code FROM public.destinations"
        iata_names = dict()
        cursor.execute(sql_query)
        records = cursor.fetchall()
        for row in records:
            iata_names[row[1]] = row[2]
    finally:
        key_ = "tasks:{0}:end".format(task_def['tech_name'])
        redis_connect.set(key_, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

        key_ = "tasks:{0}:status".format(task_def['tech_name'])
        redis_connect.set(key_, "END")
        if (connection):
            cursor.close()
            connection.close()

    # get CSV data
    #csv_file = "all_flights_auto_daily_15weeks_200usd.csv"
    csv_data = export_data_from_file(export_rows, hebr_days, iata_names)
    with open(task_def["file_name"], 'w') as file:
        file.write(csv_data)
        csv_data_absol = os.path.abspath(task_def["file_name"])

    # send email
    text_data = 'Hi'
    text_data += "\nToday {0} we get {1} new records".format(datetime.datetime.today(), count_rec)
    logger.info(text_data)
    logger.info(count_rec)
    '''https://realpython.com/python-send-email/'''
    subject = "Automation to file “{0}“".format(task_def["file_name"])
    body = "Automation to file “{0}’ is done.".format(task_def["file_name"])
    body += "\n{0} rows.".format(count_rec)
    sender_email = "postmaster@mailgun.hoho.tel"
    #receiver_email = "vintello@ukr.net"
    receiver_email = "elad@bytheweb.co.il"
    passw = "18eb9523ab9f0964edba81adb4624db1-07a637b8-339e0164"

    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Bcc"] = receiver_email  # Recommended for mass emails

    # Add body to email
    message.attach(MIMEText(body, "plain"))

    with open(csv_data_absol, "rb") as attachment:
        # Add file as application/octet-stream
        # Email client can usually download this automatically as attachment
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    # Encode file in ASCII characters to send by email
    encoders.encode_base64(part)

    # Add header as key/value pair to attachment part
    part.add_header(
        "Content-Disposition",
        "attachment; filename= {0}".format(task_def["file_name"]),
    )

    # Add attachment to message and convert message to string
    message.attach(part)
    text = message.as_string()

    with smtplib.SMTP("smtp.eu.mailgun.org", 587) as server:
        #server.set_debuglevel(1)
        server.login(sender_email, passw)
        server.sendmail(sender_email, receiver_email, text)

def NOW_MT():
    dt = datetime.datetime.now().replace(tzinfo=pytz.timezone('Asia/Jerusalem'))
    print(dt)
    return dt

''''@celery.on_after_configure.connect
def add_periodic(sender, **kwargs):
    """
    RUS
        формирование периодических задач. генерируется из определения задач .
        придобавлении новой задачи или изменения требуется перезапуск celery
        :param kwargs: None
        :return: None
    """
    list_task = generate_task_list()
    for task_ in list_task:
        if task_.get("cron_run", None):
            crn_shed = task_.get("cron_run", None)
            if type(crn_shed) == dict:
                minu = shd_item.get("minute").replace("*", "_") if shd_item.get("minute") else "_"
                houru = shd_item.get("hour").replace("*", "_") if shd_item.get("hour") else "_"
                name = "{0}_task_{1}_min_{2}_hour".format(task_.get("tech_name"),
                                                          minu,
                                                          houru)
                #print(name)
                sender.add_periodic_task(crontab(**crn_shed, nowfun=NOW_MT),
                                         week15_xxx_baks.s(task_["id"]),
                                         name=name
                                         )
            elif type(crn_shed) == list:
                for shd_item in crn_shed:
                    minu = str(shd_item.get("minute")).replace("*", "_") if shd_item.get("minute") else "_"
                    houru = str(shd_item.get("hour")).replace("*", "_")  if shd_item.get("hour") else "_"
                    name = "{0}_task_{1}_min_{2}_hour".format(task_.get("tech_name"),
                                                              minu,
                                                              houru)
                    #print(name)
                    print(crontab(**shd_item, nowfun=NOW_MT))
                    sender.add_periodic_task(crontab(**shd_item, nowfun=NOW_MT),
                                             week15_xxx_baks.s(task_["id"]),
                                             name=name
                                             )

    #celery.add_periodic_task(crontab(minute=0, hour='*/1'), week15_xxx_baks.s(2), name="week15_500baks_task")
'''

def export_data_from_file(export_rows, hebr_days, iata_names):
    csv_arr = list()
    for export_row in export_rows:
        name_row = name_generator(export_row["origin_airport"],
                                  export_row["destination_airport"],
                                  export_row["departure_at"],
                                  export_row["return_at"],
                                  export_row["price"],
                                  export_row.get("direct", None),
                                  )
        data = {"name": name_row,
                "origin_airport": export_row["origin_airport"],
                "origin_name": iata_names.get(export_row["origin_airport"], "--"),
                "destination_airport": export_row["destination_airport"],
                "destination_name": iata_names.get(export_row["destination"], "--"),
                "departure_date": export_row["departure_at"].strftime("%Y/%m/%d"),
                "departure_time": export_row["departure_at"].strftime("%H:%M"),
                "departure_dayoftheweek": get_dey_of_week(hebr_days, export_row["departure_at"]),
                "return_date": export_row["return_at"].strftime("%Y/%m/%d"),
                "return_time": export_row["return_at"].strftime("%H:%M"),
                "return_dayoftheweek": get_dey_of_week(hebr_days, export_row["return_at"]),
                "total_nights": abs((export_row["return_at"] - export_row["departure_at"]).days),
                "price": export_row["price"],
                "direct": export_row["direct"]
                }
        csv_row = "{name},{origin_airport},{origin_name},{destination_airport},{destination_name},".format(**data)
        csv_row += "{departure_date},{departure_time},{departure_dayoftheweek},{return_date},{return_time},".format(
            **data)
        csv_row += "{return_dayoftheweek},{total_nights},{price}".format(**data)
        csv_arr.append(csv_row)
    csv = ("\n").join(csv_arr)
    return csv

if __name__ == "__main__":
    connection = psycopg2.connect(user="user2",
                                  password="XmLuwoRTaPBB",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="travel")
    templ = {'origin': 'TLV',
             'departure_at': None,
             'return_at': None,
             'max_price': 2000,
             'transfers': 0,
             'return_transfers': 0,
             'min_nights_duration': 1,
             'max_nights_duration': 30,
             'start_day': None,
             'end_day': None
             }
    res = fill_dates_v3(connection,  templ, 52)
    len_res = len(res)
    set_mass = set()
    for row in res:
        str_key = row['origin']+row['destination']+row['departure_at']+row['return_at']+row['direct']
        set_mass.add(str_key)
    len_set_mass = len(set_mass)
    dd = 0