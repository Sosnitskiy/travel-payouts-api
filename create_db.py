from project import db, create_app, models
from werkzeug.security import generate_password_hash, check_password_hash
import project

app=create_app()
app.app_context().push()
db.create_all(app=app)

try:
    new_user = models.User(email="superadmin@example.com",
                           name="superadmin",
                           password=generate_password_hash('6Kqw&FRn7hc9', method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()
except Exception:
    db.session.rollback()

try:
    days_list = [('sunday','א'), ('monday','ב'), ('tuesday','ג'), ('wednesday','ד'), ('thursday','ה'), ('friday','ו'), ('saturday','ש')]
    for day_ in days_list:
        row_day = models.Weekdays(name=day_[0], hebr_day=day_[1])
        db.session.add(row_day)
    db.session.commit()
except Exception:
    db.session.rollback()

try:
    files_csv = models.File_csv(id=0, name="buffer")
    db.session.add(files_csv)
    files_csv = models.File_csv(id=1, name="all_flights.csv")
    db.session.add(files_csv)
    db.session.commit()
except Exception:
    db.session.rollback()
