Install PostgreSQL in Linux using the command,

    sudo apt-get install postgresql postgresql-contrib

install redis:

    sudo snap install redis

Now create a database for PostgreSQL

    sudo -iu postgres psql
    

And create a database using created user account

    CREATE DATABASE travel;
    CREATE USER user2 WITH PASSWORD 'XmLuwoRTaPBB';
    GRANT ALL PRIVILEGES ON DATABASE travel TO user2;
    ALTER DATABASE travel OWNER TO user2;

clone git repository git@gitlab.com:Sosnitskiy/travel-payouts-api.git

    cd /var/www/
    git clone git@gitlab.com:Sosnitskiy/travel-payouts-api.git
    cd travel-payouts-api

migrations

    export FLASK_APP=run.py

    flask db init
    flask db migrate
    flask db upgrade

create venv
    
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt

create user and change permissions
    
    sudo useradd -g www-data user1
    sudo passwd user1
    sudo chown -R user1:www-data /var/www/travel-payouts-api

copy service config file

    cp travelapi.service /etc/systemd/system/travelapi.service
    systemctl enable travelapi
    systemctl start travelapi

run celery as demonize

    cp celeryd.service /etc/systemd/system/celeryd.service
    systemctl enable celeryd
    systemctl start celeryd

    cp celeryd_low.service /etc/systemd/system/celeryd_low.service
    systemctl enable celeryd_low
    systemctl start celeryd_low

    cp celery_beat.service/etc/systemd/system/celery_beat.service
    systemctl enable celery_beat
    systemctl start celery_beat

config nginx

    cp nginx_travelapi.conf /etc/nginx/sites-available/nginx_travelapi.conf 
    sudo ln -s /etc/nginx/sites-available/nginx_travelapi.conf /etc/nginx/sites-enabled/
    nginx -t
    sudo systemctl restart nginx
    sudo systemctl status nginx


service command

    redis-cli flushall




